package rules;


import com.ifsworld.fnd.metadata.cache.model.dto.Container;
import com.ifsworld.fnd.metadata.cache.model.dto.EntitySet;
import com.ifsworld.fnd.metadata.cache.model.dto.Projection;
import result.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Sajith Jayasekara on 7/12/2017.
 */
public class Rule02 implements ProjectionRule {

    private int code = 1;
    private String message;
    private String name = "Rule02";
    private String description ="EntitySet name's first letter should be uppercase and length must be less than 30";
    ArrayList<Result> list = new ArrayList<Result>();

    @Override
    public ArrayList<Result> run(Projection projection) {


        ArrayList <EntitySet> entitySets = (ArrayList<EntitySet>) projection.getContainer().getEntitySets();

        for(EntitySet entitySet:entitySets){

            String enSetName = entitySet.getName();

                if(!Character.isUpperCase(enSetName.charAt(0))){
                    message = "EntitySet : "+enSetName +" ,"+" first letter should be Uppercase  ";
                    Result result = new Result(code,message);
                    result.setErrorMessage(enSetName+"/"+message);
                    result.setObject(entitySet);
                    list.add(result);


                }
                else {
                    if (enSetName.length()>30){
                        message = "EntitySet : "+enSetName +" ,"+"length should less than 30 ";
                        Result result = new Result(code,message);
                        result.setErrorMessage(enSetName+"/"+message);
                        result.setObject(entitySet);
                        list.add(result);

                    }
                }


        }

        return list;

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }


}
