package rules;


import com.ifsworld.fnd.metadata.cache.model.dto.Container;
import com.ifsworld.fnd.metadata.cache.model.dto.EntitySet;
import com.ifsworld.fnd.metadata.cache.model.dto.EntityType;
import com.ifsworld.fnd.metadata.cache.model.dto.Projection;
import result.Result;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Sajith Jayasekara on 7/12/2017.
 */
public class Rule03 implements ProjectionRule {


    private int code = 1;
    private String message;
    private String name = "Rule03";
    private String description = "Every EntitySet must have valid EntityType";
    ArrayList<Result> list = new ArrayList<Result>();


    @Override
    public ArrayList<Result> run(Projection projection) {

        int count = 0;


        ArrayList<EntitySet> entitySets = (ArrayList<EntitySet>) projection.getContainer().getEntitySets();

        ArrayList<EntityType> entityTypes = (ArrayList<EntityType>) projection.getEntities();

        for(EntitySet entitySet:entitySets){

            String enSetTypeName = entitySet.getEntityType();
            String enSetName = entitySet.getName();

            for(EntityType entityType:entityTypes){

                String enTypeName = entityType.getName();

                if(enTypeName.equals(enSetTypeName)){
                    count++;
                }

            }

            if (count==0){

                message ="EntitySet : " +enSetName+"'s"+" EntityType :" +enSetTypeName+" is not a valid entity type";
                Result result = new Result(code,message);
                result.setErrorMessage(enSetTypeName+"/"+message);
                result.setObject(entitySet);
                list.add(result);


            }
            else {
                count=0;
            }
        }

        return list;

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }


}
