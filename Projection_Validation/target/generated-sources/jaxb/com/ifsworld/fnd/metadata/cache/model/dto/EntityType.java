//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.07.10 at 10:43:33 AM IST 
//


package com.ifsworld.fnd.metadata.cache.model.dto;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EntityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EntityType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{dto.model.cache.metadata.fnd.ifsworld.com}ContainerBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Execute" type="{dto.model.cache.metadata.fnd.ifsworld.com}ExecutionDetails" minOccurs="0"/&gt;
 *         &lt;element name="Navigation" type="{dto.model.cache.metadata.fnd.ifsworld.com}Navigation" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Keys" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/&gt;
 *         &lt;element name="GroupBy" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="KeysWhere" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attributes" type="{dto.model.cache.metadata.fnd.ifsworld.com}Attribute" maxOccurs="unbounded"/&gt;
 *         &lt;element name="Computed" type="{dto.model.cache.metadata.fnd.ifsworld.com}Attribute" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntityType", propOrder = {
    "name",
    "execute",
    "navigation",
    "keys",
    "groupBy",
    "keysWhere",
    "attributes",
    "computed"
})
public class EntityType
    extends ContainerBase
{

    @XmlElement(name = "Name", required = true)
    protected String name;
    @XmlElement(name = "Execute")
    protected ExecutionDetails execute;
    @XmlElement(name = "Navigation")
    protected List<Navigation> navigation;
    @XmlElement(name = "Keys", required = true)
    protected List<String> keys;
    @XmlElement(name = "GroupBy")
    protected List<String> groupBy;
    @XmlElement(name = "KeysWhere")
    protected String keysWhere;
    @XmlElement(name = "Attributes", required = true)
    protected List<Attribute> attributes;
    @XmlElement(name = "Computed")
    protected List<Attribute> computed;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the execute property.
     * 
     * @return
     *     possible object is
     *     {@link ExecutionDetails }
     *     
     */
    public ExecutionDetails getExecute() {
        return execute;
    }

    /**
     * Sets the value of the execute property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutionDetails }
     *     
     */
    public void setExecute(ExecutionDetails value) {
        this.execute = value;
    }

    /**
     * Gets the value of the navigation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the navigation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNavigation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Navigation }
     * 
     * 
     */
    public List<Navigation> getNavigation() {
        if (navigation == null) {
            navigation = new ArrayList<Navigation>();
        }
        return this.navigation;
    }

    /**
     * Gets the value of the keys property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the keys property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKeys().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getKeys() {
        if (keys == null) {
            keys = new ArrayList<String>();
        }
        return this.keys;
    }

    /**
     * Gets the value of the groupBy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the groupBy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupBy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getGroupBy() {
        if (groupBy == null) {
            groupBy = new ArrayList<String>();
        }
        return this.groupBy;
    }

    /**
     * Gets the value of the keysWhere property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeysWhere() {
        return keysWhere;
    }

    /**
     * Sets the value of the keysWhere property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeysWhere(String value) {
        this.keysWhere = value;
    }

    /**
     * Gets the value of the attributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Attribute }
     * 
     * 
     */
    public List<Attribute> getAttributes() {
        if (attributes == null) {
            attributes = new ArrayList<Attribute>();
        }
        return this.attributes;
    }

    /**
     * Gets the value of the computed property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the computed property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComputed().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Attribute }
     * 
     * 
     */
    public List<Attribute> getComputed() {
        if (computed == null) {
            computed = new ArrayList<Attribute>();
        }
        return this.computed;
    }

}
