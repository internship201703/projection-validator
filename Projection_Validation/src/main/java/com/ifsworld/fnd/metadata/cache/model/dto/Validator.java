package com.ifsworld.fnd.metadata.cache.model.dto;

/**
 * Created by Sajith Jayasekara on 7/10/2017.
 */

import errorlistHandler.ErrorListHandler;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import outputGenerator.OutputGenerator1;
import outputGenerator.OutputGenerator2;
import result.Result;
import rules.ProjectionRule;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

public class Validator {



    public static void main(String[] args) throws JAXBException ,FileNotFoundException{

      File file = new File(args[0]);
      InputStream inputStream = new FileInputStream(file);

      //hashMap for map rules and its list of results.
      HashMap<ProjectionRule,ArrayList<Result>> rulesAndResults = new HashMap<>();


        final JAXBContext jaxbContext = JAXBContext.newInstance(Projection.class);
        final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE,"application/json");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT,true);
        StreamSource json = new StreamSource(inputStream);
        Projection projection = unmarshaller.unmarshal(json,Projection.class).getValue();

      //execute set of rules
      Properties property = new Properties();

      if(args.length>1) {
        try {

          property.load(new FileInputStream(args[1]));

          for (String key : property.stringPropertyNames()) {

            String value = property.getProperty(key);
            Class<ProjectionRule> cls = (Class<ProjectionRule>) Class.forName(value);
            ProjectionRule rule = cls.newInstance();
            rulesAndResults.put(rule, rule.run(projection));

          }

        } catch (IOException e) {
          e.printStackTrace();
        } catch (IllegalAccessException e) {
          e.printStackTrace();
        } catch (InstantiationException e) {
          e.printStackTrace();
        } catch (ClassNotFoundException e) {
          e.printStackTrace();
        }
      }

      ErrorListHandler errorListHandler = new ErrorListHandler();
      errorListHandler.handler(rulesAndResults);

        if(args.length > 2) {

            if (args[2].equals("OutputGenerator1")) {
                OutputGenerator1 outputGenerator1 = new OutputGenerator1();
                outputGenerator1.printOutput(projection,rulesAndResults);
            } else if (args[2].equals("OutputGenerator2")) {
                OutputGenerator2 outputGenerator2 = new OutputGenerator2();
                outputGenerator2.printOutput(projection,rulesAndResults);
            } else {
                System.out.println("Please enter valid OutputGenerator ");
            }
        }
        else {
            System.out.println("Please enter valid OutputGenerator as a argument ");
        }






    }
}
