package outputGenerator;

import com.ifsworld.fnd.metadata.cache.model.dto.*;
import result.Result;
import rules.ProjectionRule;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * Created by Sajith Jayasekara on 9/12/2017.
 */
public class OutputGenerator2 implements outputGenerator {
    @Override
    public void printOutput(Projection projection, HashMap<ProjectionRule, ArrayList<Result>> rulesAndResults) {

        File file=new File("NewOutput2.html");

        if(file.exists()){
            file.delete();
        }

        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            bufferedWriter.write("<html>");
            bufferedWriter.write("<head>");
            bufferedWriter.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            bufferedWriter.write("<link rel=\"stylesheet\" href=\"src/main/js/bootstrap.min.css\"></script>");
            bufferedWriter.write("<script src=\"src/main/js/jquery-3.2.1.min.js\"></script>");
            bufferedWriter.write("<script src=\"src/main/js/bootstrap.min.js\"></script>");
            bufferedWriter.write("<script src=\"src/main/js/Script.js\"></script>");
            bufferedWriter.write("<style>" +
                    "#sec1{text-indent: 20px}"+
                    "</style>" +
                    "<style>" +
                    "#sec2{text-indent: 50px}"+
                    "</style>" +
                    "<style>" +
                    "#sec3{text-indent: 100px}"+
                    "</style>" +
                    "<style>" +
                    "#sec4{text-indent: 175px}"+
                    "</style>" +
                    "<style>" +
                    "#secForSQL{text-indent: 120px}"+
                    "</style>" +
                    "<style>" +
                    "#sec5{text-indent: 200px}"+
                    "</style>"
            );
            bufferedWriter.write("</head>");
            bufferedWriter.write("<body style=\"background-color:#00BFFF;\">");
            bufferedWriter.write("<div><center><h1 style=\"color:white; font-weight: bold\">JSON STRUCTURE</h1></center></div>");
            bufferedWriter.write("<div class=\"container \">");
            bufferedWriter.write("<div  style=\"background-color: #FFFFFF; font-weight: bold\">");
            //projection start
            // bufferedWriter.write("<button type=\"button\" id=\"button1\" class=\"btn btn-default btn-xs\" data-toggle=\"collapse\" data-target=\"#demo\">-</button>");
            bufferedWriter.write("<br>");
            bufferedWriter.write("<div id=\"demo\" class=\"collapse in col-sm-offset-1\"><p>Projection {</p>");

            if(projection.getErrorList().size()>0){
                bufferedWriter.write("<p>"+"Name : "+"<span style=\"color:red;\">"+projection.getName()+"</span>"+"</p>");
            }
            else {
                bufferedWriter.write("<p>"+"Name : "+projection.getName()+"</p>");
            }

            bufferedWriter.write("<p>"+"Version : "+projection.getMetadata().getVersion()+"</p>");
            //start container
            bufferedWriter.write("<button type=\"button\" id=\"button2\" class=\"btn btn-default btn-xs\" data-toggle=\"collapse\" data-target=\"#container\">-</button>");
            bufferedWriter.write("<p id=\"sec1\">Container : {</p>");
            bufferedWriter.write("<div id=\"container\" class=\"collapse in\">");
            bufferedWriter.write("<br>");
            //start of entity sets
            bufferedWriter.write("<button type=\"button\" id=\"button6\" class=\"btn btn-default btn-xs\" data-toggle=\"collapse\" data-target=\"#entitySets\">-</button>");
            bufferedWriter.write("<p id=\"sec2\">EntitySets : [</p>");
            bufferedWriter.write("<div id=\"entitySets\" class=\"collapse in\">");
            for(EntitySet entitySets:projection.getContainer().getEntitySets()){
                int countForName = 0;
                int countForType = 0;
                String name = entitySets.getName();
                String type = entitySets.getEntityType();
                for(String error:entitySets.getErrorList()){
                    StringTokenizer stringTokenizer = new StringTokenizer(error,"/");
                    String part = stringTokenizer.nextToken();
                    if(part.equals(name)){
                        countForName++;
                    }
                    else if(part.equals(type)){
                        countForType++;
                    }
                }
                bufferedWriter.write("<p style=\"text-indent: 75px\">{</p>");
                if(countForName>0){
                    bufferedWriter.write("<p id=\"sec3\">"+"Name : "+"<span style=\"color:red;\">"+name+"</span>"+"</p>");
                }
                else {
                    bufferedWriter.write("<p id=\"sec3\">"+"Name : "+name+"</p>");
                }

                if(countForType>0){
                    bufferedWriter.write("<p id=\"sec3\">"+"EntityType : "+"<span style=\"color:red;\">"+type+"</span>"+"</p>");
                }
                else {
                    bufferedWriter.write("<p id=\"sec3\">"+"EntityType : "+type+"</p>");
                }
                bufferedWriter.write("<p style=\"text-indent: 75px\">}</p>");

            }
            bufferedWriter.write("<br>");
            bufferedWriter.write("</div>");
            //end of entitySets
            bufferedWriter.write("<p id=\"sec2\">],</p>");
            bufferedWriter.write("<br>");
            //start container singleton
            bufferedWriter.write("<button type=\"button\" id=\"button7\" class=\"btn btn-default btn-xs\" data-toggle=\"collapse\" data-target=\"#singletons\">-</button>");
            bufferedWriter.write("<p id=\"sec2\">Singletons : [</p>");
            bufferedWriter.write("<div id=\"singletons\" class=\"collapse in\">");
            bufferedWriter.write("<br>");
            //end of singleton
            bufferedWriter.write("</div>");
            bufferedWriter.write("<p id=\"sec2\">],</p>");
            bufferedWriter.write("<br>");
            //start container action
            bufferedWriter.write("<button type=\"button\" id=\"button8\" class=\"btn btn-default btn-xs\" data-toggle=\"collapse\" data-target=\"#actions\">-</button>");
            bufferedWriter.write("<p id=\"sec2\">Actions : [</p>");
            bufferedWriter.write("<div id=\"actions\" class=\"collapse in\">");
            bufferedWriter.write("<div id=\"sec3\">");

            for (Action actions:projection.getContainer().getActions()){
                bufferedWriter.write("<p style=\"text-indent: 75px\">{</p>");
                bufferedWriter.write("<p> Name :"+actions.getName()+"</p>");
                bufferedWriter.write("<p> DataType :"+actions.getDataType()+"</p>");
                bufferedWriter.write("<p> SubType :"+actions.getSubType()+"</p>");
                if(actions.isCollection()){
                    bufferedWriter.write("<p>"+"Collection : "+"true"+"</p>");
                }
                else {
                    bufferedWriter.write("<p>"+"Collection : "+"false"+"</p>");
                }
                bufferedWriter.write("<p> Parameters : [</p>");
                bufferedWriter.write("<div id=\"sec4\">");
                for (Parameter parameter : actions.getParameters()){
                    bufferedWriter.write("<p style=\"text-indent: 120px\">{</p>");
                    bufferedWriter.write("<p>"+"Name : "+parameter.getName()+"</p>");
                    bufferedWriter.write("<p>"+"DataType : "+parameter.getDataType()+"</p>");
                    if(parameter.isCollection()){
                        bufferedWriter.write("<p>"+"Collection : "+"true"+"</p>");
                    }
                    else {
                        bufferedWriter.write("<p>"+"Collection : "+"false"+"</p>");
                    }
                    if(parameter.isNullable()){
                        bufferedWriter.write("<p>"+"Nullable : "+"true"+"</p>");
                    }
                    else {
                        bufferedWriter.write("<p>"+"Nullable : "+"false"+"</p>");
                    }
                    bufferedWriter.write("<p style=\"text-indent: 120px\">}</p>");
                }

                bufferedWriter.write("</div>");
                bufferedWriter.write("<p>] ,</p>");
                bufferedWriter.write("<p> Execute : {</p>");
                if(actions.getExecute().getSQL() != null) {
                    bufferedWriter.write("<p id=\"secForSQL\"> SQL : {</p>");
                    String where = actions.getExecute().getSQL().getWhere();

                    if(where != null){
                        bufferedWriter.write("<p id=\"sec4\">"+"Where : "+where+"</p>");
                    }
                    else {
                        bufferedWriter.write("<p id=\"sec4\">"+"From : "+actions.getExecute().getSQL().getFrom()+"</p>");
                    }
                    bufferedWriter.write("<p id=\"sec4\">"+"Bind : [</p>");
                    bufferedWriter.write("<div id=\"sec5\">");
                    for(Bind bind : actions.getExecute().getSQL().getBind()){
                        bufferedWriter.write("<p style=\"text-indent: 190px\">}</p>");
                        bufferedWriter.write("<p>"+"Kind : "+bind.getKind()+"</p>");
                        bufferedWriter.write("<p>"+"Name : "+bind.getName()+"</p>");
                        bufferedWriter.write("<p>"+"ImplementationType : "+bind.getImplementationType()+"</p>");
                        bufferedWriter.write("<p style=\"text-indent: 190px\">}</p>");
                    }
                    bufferedWriter.write("</div>");
                    bufferedWriter.write("<p id=\"sec4\">]</p>");
                    bufferedWriter.write("<p id=\"secForSQL\">}</p>");
                }
                else {
                    bufferedWriter.write("<p id=\"secForSQL\"> PL-SQL : {</p>");
                    bufferedWriter.write("<p id=\"sec4\">"+"Bind : [</p>");
                    bufferedWriter.write("<div id=\"sec5\">");
                    for(Bind bind : actions.getExecute().getPLSQL().getBind()){
                        bufferedWriter.write("<p style=\"text-indent: 190px\">}</p>");
                        bufferedWriter.write("<p>"+"Kind : "+bind.getKind()+"</p>");
                        bufferedWriter.write("<p>"+"Name : "+bind.getName()+"</p>");
                        bufferedWriter.write("<p>"+"ImplementationType : "+bind.getImplementationType()+"</p>");
                        bufferedWriter.write("<p style=\"text-indent: 190px\">}</p>");
                    }
                    bufferedWriter.write("</div>");
                    bufferedWriter.write("<p id=\"sec4\">]</p>");
                    bufferedWriter.write("<p id=\"secForSQL\">}</p>");
                }
                bufferedWriter.write("<p>}</p>");

                bufferedWriter.write("<p style=\"text-indent: 75px\">},</p>");
            }

            bufferedWriter.write("<br>");
            bufferedWriter.write("</div>");
            bufferedWriter.write("</div>");
            //end action
            bufferedWriter.write("<p id=\"sec2\">],</p>");
            bufferedWriter.write("<br>");

            //start container functions
            bufferedWriter.write("<button type=\"button\" id=\"button9\" class=\"btn btn-default btn-xs\" data-toggle=\"collapse\" data-target=\"#functions\">-</button>");
            bufferedWriter.write("<p id=\"sec2\">Functions : [</p>");
            bufferedWriter.write("<div id=\"functions\" class=\"collapse in\">");
            bufferedWriter.write("<div id=\"sec3\">");

            for(Function function:projection.getContainer().getFunctions()){

                int countForName = 0;
                String functionName = function.getName();

                for(String error : function.getErrorList()){
                    StringTokenizer stringTokenizer = new StringTokenizer(error,"/");
                    String part = stringTokenizer.nextToken();
                    if(part.equals(functionName)){
                        countForName++;
                    }
                }
                bufferedWriter.write("<p style=\"text-indent: 75px\">{</p>");
                if(countForName>0){
                    bufferedWriter.write("<p>"+"Name : "+"<span style=\"color:red;\">"+functionName+"</span>"+"</p>");
                }
                else {
                    bufferedWriter.write("<p>"+"Name : "+functionName+"</p>");
                }

                bufferedWriter.write("<p>"+"DataType : "+function.getDataType()+"</p>");
                bufferedWriter.write("<p>"+"Implementation : "+function.getImplementation()+"</p>");
                bufferedWriter.write("<p>"+"Bean : "+function.getBean()+"</p>");
                bufferedWriter.write("<p>"+"SubType : "+function.getSubType()+"</p>");
                if(function.isCollection()){
                    bufferedWriter.write("<p>"+"Collection : "+"true"+"</p>");
                }
                else {
                    bufferedWriter.write("<p>"+"Collection : "+"false"+"</p>");
                }
                bufferedWriter.write("<p> Parameters : [</p>");
                bufferedWriter.write("<div id=\"sec4\">");
                for (Parameter parameter : function.getParameters()){
                    bufferedWriter.write("<p style=\"text-indent: 120px\">{</p>");
                    bufferedWriter.write("<p>"+"Name : "+parameter.getName()+"</p>");
                    bufferedWriter.write("<p>"+"DataType : "+parameter.getDataType()+"</p>");
                    if(parameter.isCollection()){
                        bufferedWriter.write("<p>"+"Collection : "+"true"+"</p>");
                    }
                    else {
                        bufferedWriter.write("<p>"+"Collection : "+"false"+"</p>");
                    }
                    if(parameter.isNullable()){
                        bufferedWriter.write("<p>"+"Nullable : "+"true"+"</p>");
                    }
                    else {
                        bufferedWriter.write("<p>"+"Nullable : "+"false"+"</p>");
                    }
                    bufferedWriter.write("<p style=\"text-indent: 120px\">}</p>");
                }

                bufferedWriter.write("</div>");
                bufferedWriter.write("<p>] ,</p>");
                bufferedWriter.write("<p> Execute : {</p>");
                bufferedWriter.write("<p id=\"secForSQL\"> SQL : {</p>");
                String where = function.getExecute().getSQL().getWhere();
                if(where!= null && countForName >0){
                    bufferedWriter.write("<p id=\"sec4\">"+"Where : "+"<span style=\"color:red;\">"+where+"</span></p>");
                }
                else  if(where!= null){
                    bufferedWriter.write("<p id=\"sec4\">"+"Where : "+where+"</p>");
                }
                else {
                    bufferedWriter.write("<p id=\"sec4\">"+"From : "+function.getExecute().getSQL().getFrom()+"</p>");
                }
                bufferedWriter.write("<p id=\"sec4\">"+"Bind : [</p>");
                bufferedWriter.write("<div id=\"sec5\">");
                for(Bind bind : function.getExecute().getSQL().getBind()){
                    bufferedWriter.write("<p style=\"text-indent: 190px\">{</p>");
                    bufferedWriter.write("<p>"+"Kind : "+bind.getKind()+"</p>");
                    bufferedWriter.write("<p>"+"Name : "+bind.getName()+"</p>");
                    bufferedWriter.write("<p>"+"ImplementationType : "+bind.getImplementationType()+"</p>");
                    bufferedWriter.write("<p style=\"text-indent: 190px\">}</p>");
                }
                bufferedWriter.write("</div>");
                bufferedWriter.write("<p id=\"sec4\">]</p>");
                bufferedWriter.write("<p id=\"secForSQL\">}</p>");
                bufferedWriter.write("<p>}</p>");

                bufferedWriter.write("<p style=\"text-indent: 75px\">},</p>");
                bufferedWriter.write("<br>");
            }
            bufferedWriter.write("</div>");
            bufferedWriter.write("<br>");
            bufferedWriter.write("</div>");
            bufferedWriter.write("<p id=\"sec2\">]</p>");
            bufferedWriter.write("<br>");
            bufferedWriter.write("</div>");
            //end of container
            bufferedWriter.write("<p id=\"sec1\">},</p>");
            bufferedWriter.write("<br>");
            //start Enumerations
            bufferedWriter.write("<button type=\"button\" id=\"button3\" class=\"btn btn-default btn-xs\" data-toggle=\"collapse\" data-target=\"#enumerations\">-</button>");
            bufferedWriter.write("<p id=\"sec1\">Enumerations : [</p>");
            bufferedWriter.write("<div id=\"enumerations\" class=\"collapse in\">");
            for (EnumerationType enumerationType: projection.getEnumerations()){
                bufferedWriter.write("<p id=\"sec2\">{</p>");
                bufferedWriter.write("<div id=\"sec3\">");
                bufferedWriter.write("<p> Name : "+enumerationType.getName()+"</p>");
                bufferedWriter.write("<p>Values : [</P>");

                for (String value:enumerationType.getValues()){

                }
                bufferedWriter.write("<p>]</P>");
                bufferedWriter.write("</div>");
                bufferedWriter.write("<p id=\"sec2\">}</p>");
            }
            bufferedWriter.write("<br>");
            bufferedWriter.write("</div>");
            bufferedWriter.write("<p id=\"sec1\">],</p>");
            //end Enumerations
            bufferedWriter.write("<br>");
            //start Structures
            bufferedWriter.write("<button type=\"button\" id=\"button4\" class=\"btn btn-default btn-xs\" data-toggle=\"collapse\" data-target=\"#structures\">-</button>");
            bufferedWriter.write("<p id=\"sec1\">Structures : [</p>");
            bufferedWriter.write("<div id=\"structures\" class=\"collapse in\">");
            for (StructureType structureType:projection.getStructures()){
                bufferedWriter.write("<p id=\"sec2\">{</p>");
                bufferedWriter.write("<div id=\"sec3\">");
                bufferedWriter.write("<p> Name : "+structureType.getName()+"</p>");
                bufferedWriter.write("<p> PLSQLRecordName : "+structureType.getPLSQLRecordName()+"</p>");
                bufferedWriter.write("<p> Attributes [ </p>");
                for (Attribute attribute:structureType.getAttributes()){
                    bufferedWriter.write("<p style=\"text-indent: 120px\">{</p>");
                    bufferedWriter.write("<div id=\"sec4\">");
                    bufferedWriter.write("<p> Name : "+attribute.getName()+"</p>");
                    bufferedWriter.write("<p>DataType : "+attribute.getDataType()+"</p>");
                    if(attribute.isCollection()){
                        bufferedWriter.write("<p>"+"Collection : "+"true"+"</p>");
                    }
                    else {
                        bufferedWriter.write("<p>"+"Collection : "+"false"+"</p>");
                    }

                    if (attribute.isNullable()){
                        bufferedWriter.write("<p>"+"Nullable : "+"true"+"</p>");
                    }else{
                        bufferedWriter.write("<p>"+"Nullable : "+"false"+"</p>");
                    }
                    bufferedWriter.write("<p> AttrName : "+attribute.getAttrName()+"</p>");
                    if(attribute.getExecute() != null) {
                        bufferedWriter.write("<p>Execute : {</p>");
                        bufferedWriter.write("<p style=\"text-indent: 185px\">SQL : {</p>");
                        bufferedWriter.write("<p id=\"sec5\">ImplementationType : " + attribute.getExecute().getSQL().getImplementationType() + "</p>");
                        bufferedWriter.write("<p style=\"text-indent: 185px\">}</p>");
                        bufferedWriter.write("<p>}</p>");
                    }
                    bufferedWriter.write("</div>");
                    bufferedWriter.write("<p style=\"text-indent: 120px\">},</p>");
                }
                bufferedWriter.write("<p>]</p>");
                bufferedWriter.write("</div>");
                bufferedWriter.write("<p id=\"sec2\">},</p>");
            }
            bufferedWriter.write("<br>");
            bufferedWriter.write("</div>");
            bufferedWriter.write("<p id=\"sec1\">],</p>");
            //end Structures
            bufferedWriter.write("<br>");
            //start Entities
            bufferedWriter.write("<button type=\"button\" id=\"button5\" class=\"btn btn-default btn-xs\" data-toggle=\"collapse\" data-target=\"#entities\">-</button>");
            bufferedWriter.write("<p id=\"sec1\">Entities : [</p>");
            bufferedWriter.write("<div id=\"entities\" class=\"collapse in\">");
            bufferedWriter.write("<br>");
            for(EntityType entityType:projection.getEntities()) {
                bufferedWriter.write("<p style=\"text-indent: 40px\">{</p>");
                bufferedWriter.write("<p id=\"sec2\">Name : "+entityType.getName()+"</p>");
                bufferedWriter.write("<p id=\"sec2\">Execute : {</p>");
                bufferedWriter.write("<div id=\"sec3\">");
                bufferedWriter.write("<p>SQL : {</p>");
                bufferedWriter.write("<div id=\"sec4\">");
                bufferedWriter.write("<p>From : "+entityType.getExecute().getSQL().getFrom()+"</p>");
                bufferedWriter.write("</div>");
                bufferedWriter.write("<p>}</p>");
                bufferedWriter.write("</div>");
                bufferedWriter.write("<p id=\"sec2\">},</p>");
                bufferedWriter.write("<p id=\"sec2\">Keys : [</p>");
                for(String key : entityType.getKeys()){
                    bufferedWriter.write("<div id=\"sec3\">");
                    bufferedWriter.write("<p>"+key+"</p>");
                    bufferedWriter.write("</div>");
                }
                bufferedWriter.write("<p id=\"sec2\">],</p>");
                bufferedWriter.write("<p id=\"sec2\">KeysWhere : "+entityType.getKeysWhere()+"</p>");
                //start attribute
                bufferedWriter.write("<p id=\"sec2\">Attributes : [</p>");
                for(Attribute attribute : entityType.getAttributes()){
                    bufferedWriter.write("<div style=\"text-indent: 75px\">");
                    bufferedWriter.write("<p>{</p>");
                    bufferedWriter.write("<div id=\"sec3\">");
                    bufferedWriter.write("<p> Name :"+attribute.getName()+"</p>");
                    if(attribute.getImplementation() != null){
                        bufferedWriter.write("<p>Implementation : "+attribute.getImplementation()+"</p>");
                    }
                    if(attribute.getBean() != null){
                        bufferedWriter.write("<p>Bean : "+attribute.getBean()+"</p>");
                    }
                    bufferedWriter.write("<p>DataType : "+attribute.getDataType()+"</p>");
                    if(attribute.isCollection()){
                        bufferedWriter.write("<p>"+"Collection : "+"true"+"</p>");
                    }
                    else {
                        bufferedWriter.write("<p>"+"Collection : "+"false"+"</p>");
                    }
                    if(attribute.getSize() != null){
                        bufferedWriter.write("<p>Size : "+attribute.getSize()+"</p>");
                    }
                    if(attribute.getScale() != null){
                        bufferedWriter.write("<p>Scale : "+attribute.getScale()+"</p>");
                    }
                    if(attribute.getPrecision() != null){
                        bufferedWriter.write("<p>Precision : "+attribute.getPrecision()+"</p>");
                    }
                    if(attribute.getTrueValue() != null){
                        bufferedWriter.write("<p>TrueValue : "+attribute.getTrueValue()+"</p>");
                    }
                    if(attribute.getFalseValue() != null){
                        bufferedWriter.write("<p>FalseValue : "+attribute.getFalseValue()+"</p>");
                    }
                    if(attribute.getMimeType() != null){
                        bufferedWriter.write("<p>MimeType : "+attribute.getMimeType()+"</p>");
                    }
                    if (attribute.isNullable()){
                        bufferedWriter.write("<p>"+"Nullable : "+"true"+"</p>");
                    }else{
                        bufferedWriter.write("<p>"+"Nullable : "+"false"+"</p>");
                    }
                    if(attribute.getAttrName() != null){
                        bufferedWriter.write("<p>AttrName : "+attribute.getAttrName()+"</p>");
                    }
                    bufferedWriter.write("<p>Execute : { </p>");
                    bufferedWriter.write("<p id=\"secForSQL\"> SQL : {</p>");
                    bufferedWriter.write("<div id=\"sec4\">");
                    bufferedWriter.write("<p>Select : "+attribute.getExecute().getSQL().getSelect()+"</p>");
                    bufferedWriter.write("<p>ImplementationType : "+attribute.getExecute().getSQL().getImplementationType()+"</p>");
                    bufferedWriter.write("<p>Alias : "+attribute.getExecute().getSQL().getAlias()+"</p>");
                    bufferedWriter.write("</div>");
                    bufferedWriter.write("<p id=\"secForSQL\"> }</p>");
                    bufferedWriter.write("<p>}</p>");
                    bufferedWriter.write("</div>");
                    bufferedWriter.write("<p>}</p>");
                    bufferedWriter.write("</div>");
                }
                bufferedWriter.write("<p id=\"sec2\">],</p>");
                //end attribute
                //start computed
                bufferedWriter.write("<p id=\"sec2\">Computed : [</p>");
                bufferedWriter.write("<div style=\"text-indent: 75px\">");

                for(Attribute attribute:entityType.getComputed()){
                    bufferedWriter.write("<p>{</p>");
                    bufferedWriter.write("<div id=\"sec3\">");
                    bufferedWriter.write("<p> Name :"+attribute.getName()+"</p>");
                    if(attribute.getImplementation() != null){
                        bufferedWriter.write("<p>Implementation : "+attribute.getImplementation()+"</p>");
                    }
                    if(attribute.getBean() != null){
                        bufferedWriter.write("<p>Bean : "+attribute.getBean()+"</p>");
                    }
                    bufferedWriter.write("<p>DataType : "+attribute.getDataType()+"</p>");
                    if(attribute.isCollection()){
                        bufferedWriter.write("<p>"+"Collection : "+"true"+"</p>");
                    }
                    else {
                        bufferedWriter.write("<p>"+"Collection : "+"false"+"</p>");
                    }
                    if(attribute.getSize() != null){
                        bufferedWriter.write("<p>Size : "+attribute.getSize()+"</p>");
                    }
                    if(attribute.getScale() != null){
                        bufferedWriter.write("<p>Scale : "+attribute.getScale()+"</p>");
                    }
                    if(attribute.getPrecision() != null){
                        bufferedWriter.write("<p>Precision : "+attribute.getPrecision()+"</p>");
                    }
                    if(attribute.getTrueValue() != null){
                        bufferedWriter.write("<p>TrueValue : "+attribute.getTrueValue()+"</p>");
                    }
                    if(attribute.getFalseValue() != null){
                        bufferedWriter.write("<p>FalseValue : "+attribute.getFalseValue()+"</p>");
                    }
                    if(attribute.getMimeType() != null){
                        bufferedWriter.write("<p>MimeType : "+attribute.getMimeType()+"</p>");
                    }
                    if (attribute.isNullable()){
                        bufferedWriter.write("<p>"+"Nullable : "+"true"+"</p>");
                    }else{
                        bufferedWriter.write("<p>"+"Nullable : "+"false"+"</p>");
                    }
                    if(attribute.getAttrName() != null){
                        bufferedWriter.write("<p>AttrName : "+attribute.getAttrName()+"</p>");
                    }
                    bufferedWriter.write("<p>Execute : { </p>");
                    bufferedWriter.write("<p id=\"secForSQL\"> SQL : {</p>");
                    bufferedWriter.write("<div id=\"sec4\">");
                    bufferedWriter.write("<p>Select : "+attribute.getExecute().getSQL().getSelect()+"</p>");
                    bufferedWriter.write("<p>ImplementationType : "+attribute.getExecute().getSQL().getImplementationType()+"</p>");
                    bufferedWriter.write("<p>Alias : "+attribute.getExecute().getSQL().getAlias()+"</p>");
                    bufferedWriter.write("</div>");
                    bufferedWriter.write("<p id=\"secForSQL\"> }</p>");
                    bufferedWriter.write("<p>}</p>");
                    bufferedWriter.write("</div>");
                    bufferedWriter.write("<p>}</p>");
                }

                bufferedWriter.write("</div>");
                bufferedWriter.write("<p id=\"sec2\">],</p>");
                //end of computed
                //start annotations
                bufferedWriter.write("<p id=\"sec2\">Annotations : [</p>");
//                bufferedWriter.write("<div style=\"text-indent: 75px\">");
//                bufferedWriter.write("<p>{</p>");
//                bufferedWriter.write("<div id=\"sec3\">");
//                bufferedWriter.write("</div>");
//                bufferedWriter.write("<p>}</p>");
//                bufferedWriter.write("</div>");
                bufferedWriter.write("<p id=\"sec2\">],</p>");
                // end of Annotations
                //start navigation
                bufferedWriter.write("<p id=\"sec2\">Navigation : [</p>");
                bufferedWriter.write("<div style=\"text-indent: 75px\">");
                for(Navigation navigation:entityType.getNavigation()) {
                    bufferedWriter.write("<p>{</p>");
                    bufferedWriter.write("<div id=\"sec3\">");
                    bufferedWriter.write("<p> Name : "+navigation.getName()+"</p>");
                    if(navigation.isCollection()){
                        bufferedWriter.write("<p>"+"Collection : "+"true"+"</p>");
                    }
                    else {
                        bufferedWriter.write("<p>"+"Collection : "+"false"+"</p>");
                    }
                    bufferedWriter.write("<p> Target : "+navigation.getTarget()+"</p>");
                    bufferedWriter.write("<p> Keys : [</p>");
                    for (Navigation.Keys keys : navigation.getKeys()){
                        bufferedWriter.write("<div style=\"text-indent: 120px\">");
                        bufferedWriter.write("<p>{<p>");
                        bufferedWriter.write("<div id=\"sec4\">");
                        bufferedWriter.write("<p>ThisAttribute : "+keys.getThisAttribute()+"</p>");
                        bufferedWriter.write("<p>TargetAttribute : "+keys.getTargetAttribute()+"</p>");
                        bufferedWriter.write("</div>");
                        bufferedWriter.write("<p>}<p>");
                        bufferedWriter.write("</div>");

                    }
                    bufferedWriter.write("<p> ],</p>");
                    bufferedWriter.write("<p> ParentAttributes : [</p>");
                    for (String attribute: navigation.getParentAttributes()){
                        bufferedWriter.write("<div style=\"text-indent: 120px\">");
                        bufferedWriter.write("<p>{<p>");
                        bufferedWriter.write("<div id=\"sec4\">");
                        bufferedWriter.write("<p>"+attribute+"</p>");
                        bufferedWriter.write("</div>");
                        bufferedWriter.write("<p>}<p>");
                        bufferedWriter.write("</div>");
                    }
                    bufferedWriter.write("<p> ],</p>");
                    bufferedWriter.write("<p> ChildAttributes : [</p>");
                    for (String attribute: navigation.getChildAttributes()){
                        bufferedWriter.write("<div style=\"text-indent: 120px\">");
                        bufferedWriter.write("<p>{<p>");
                        bufferedWriter.write("<div id=\"sec4\">");
                        bufferedWriter.write("<p>"+attribute+"</p>");
                        bufferedWriter.write("</div>");
                        bufferedWriter.write("<p>}<p>");
                        bufferedWriter.write("</div>");
                    }
                    bufferedWriter.write("<p> ],</p>");
                    bufferedWriter.write("<p> Where : "+navigation.getWhere()+"</p>");
                    bufferedWriter.write("</div>");
                    bufferedWriter.write("<p>}</p>");

                }
                bufferedWriter.write("</div>");
                bufferedWriter.write("<p id=\"sec2\">],</p>");
                //end of Navigation
                //start curd
                bufferedWriter.write("<p id=\"sec2\">CRUD : [</p>");
                for (Action action : entityType.getCRUD()) {
                    bufferedWriter.write("<div style=\"text-indent: 75px\">");
                    bufferedWriter.write("<p>{</p>");
                    bufferedWriter.write("<div id=\"sec3\">");
                    bufferedWriter.write("<p> Name :"+action.getName()+"</p>");
                    bufferedWriter.write("<p> DataType :"+action.getDataType()+"</p>");
                    bufferedWriter.write("<p> SubType :"+action.getSubType()+"</p>");
                    if(action.isCollection()){
                        bufferedWriter.write("<p>"+"Collection : "+"true"+"</p>");
                    }
                    else {
                        bufferedWriter.write("<p>"+"Collection : "+"false"+"</p>");
                    }
                    bufferedWriter.write("<p>"+"Parameters : [</p>");
                    if(action.getParameters() != null){
                        for (Parameter parameter : action.getParameters()){
                            bufferedWriter.write("<p style=\"text-indent: 120px\">{</p>");
                            bufferedWriter.write("<p>"+"Name : "+parameter.getName()+"</p>");
                            bufferedWriter.write("<p>"+"DataType : "+parameter.getDataType()+"</p>");
                            if(parameter.isCollection()){
                                bufferedWriter.write("<p>"+"Collection : "+"true"+"</p>");
                            }
                            else {
                                bufferedWriter.write("<p>"+"Collection : "+"false"+"</p>");
                            }
                            if(parameter.isNullable()){
                                bufferedWriter.write("<p>"+"Nullable : "+"true"+"</p>");
                            }
                            else {
                                bufferedWriter.write("<p>"+"Nullable : "+"false"+"</p>");
                            }
                            bufferedWriter.write("<p style=\"text-indent: 120px\">}</p>");
                        }
                    }
                    bufferedWriter.write("<p>"+"],</p>");
                    bufferedWriter.write("<p> Execute : {</p>");
                    if(action.getExecute().getSQL() != null) {
                        bufferedWriter.write("<p id=\"secForSQL\"> SQL : {</p>");
                        String where = action.getExecute().getSQL().getWhere();

                        if(where != null){
                            bufferedWriter.write("<p id=\"sec4\">"+"Where : "+where+"</p>");
                        }
                        else {
                            bufferedWriter.write("<p id=\"sec4\">"+"From : "+action.getExecute().getSQL().getFrom()+"</p>");
                        }
                        bufferedWriter.write("<p id=\"sec4\">"+"Bind : [</p>");
                        bufferedWriter.write("<div id=\"sec5\">");
                        for(Bind bind : action.getExecute().getSQL().getBind()){
                            bufferedWriter.write("<p style=\"text-indent: 190px\">}</p>");
                            bufferedWriter.write("<p>"+"Kind : "+bind.getKind()+"</p>");
                            bufferedWriter.write("<p>"+"Name : "+bind.getName()+"</p>");
                            bufferedWriter.write("<p>"+"ImplementationType : "+bind.getImplementationType()+"</p>");
                            bufferedWriter.write("<p style=\"text-indent: 190px\">}</p>");
                        }
                        bufferedWriter.write("</div>");
                        bufferedWriter.write("<p id=\"sec4\">]</p>");
                        bufferedWriter.write("<p id=\"secForSQL\">}</p>");
                    }
                    if(action.getExecute().getPLSQL() != null){
                        bufferedWriter.write("<p id=\"secForSQL\"> PL-SQL : {</p>");
                        bufferedWriter.write("<p id=\"sec4\">"+"Code : [</p>");
                        for (String code:action.getExecute().getPLSQL().getCode()){
                            bufferedWriter.write("<div id=\"sec5\">");
                            bufferedWriter.write("<p>"+code+"</p>");
                            bufferedWriter.write("</div>");
                        }
                        bufferedWriter.write("<p id=\"sec4\">],</p>");
                        bufferedWriter.write("<p id=\"sec4\">"+"Bind : [</p>");
                        bufferedWriter.write("<div id=\"sec5\">");
                        for(Bind bind : action.getExecute().getPLSQL().getBind()){
                            bufferedWriter.write("<p style=\"text-indent: 190px\">}</p>");
                            bufferedWriter.write("<p>"+"Kind : "+bind.getKind()+"</p>");
                            bufferedWriter.write("<p>"+"Name : "+bind.getName()+"</p>");
                            bufferedWriter.write("<p>"+"ImplementationType : "+bind.getImplementationType()+"</p>");
                            bufferedWriter.write("<p style=\"text-indent: 190px\">}</p>");
                        }
                        bufferedWriter.write("</div>");
                        bufferedWriter.write("<p id=\"sec4\">]</p>");
                        bufferedWriter.write("<p id=\"secForSQL\">}</p>");
                    }
                    bufferedWriter.write("<p>}</p>");
                    //end execute
                    bufferedWriter.write("</div>");
                    bufferedWriter.write("<p>},</p>");
                    bufferedWriter.write("</div>");
                }
                bufferedWriter.write("<p id=\"sec2\">],</p>");
                //end of curd
                //start action
                bufferedWriter.write("<p id=\"sec2\">Actions : [</p>");
                for (Action action : entityType.getActions()) {
                    bufferedWriter.write("<div style=\"text-indent: 75px\">");
                    bufferedWriter.write("<p>{</p>");
                    bufferedWriter.write("<div id=\"sec3\">");

                    bufferedWriter.write("<p> Name :"+action.getName()+"</p>");
                    if(action.getDataType() != null) {
                        bufferedWriter.write("<p> DataType :" + action.getDataType() + "</p>");
                    }
                    if(action.getSubType() != null) {
                        bufferedWriter.write("<p> SubType :" + action.getSubType() + "</p>");
                    }

                    if(action.isCollection()){
                        bufferedWriter.write("<p>"+"Collection : "+"true"+"</p>");
                    }
                    else {
                        bufferedWriter.write("<p>"+"Collection : "+"false"+"</p>");
                    }
                    bufferedWriter.write("<p>"+"Parameters : [</p>");
                    if(action.getParameters() != null){
                        for (Parameter parameter : action.getParameters()){
                            bufferedWriter.write("<p style=\"text-indent: 120px\">{</p>");
                            bufferedWriter.write("<p>"+"Name : "+parameter.getName()+"</p>");
                            bufferedWriter.write("<p>"+"DataType : "+parameter.getDataType()+"</p>");
                            if(parameter.isCollection()){
                                bufferedWriter.write("<p>"+"Collection : "+"true"+"</p>");
                            }
                            else {
                                bufferedWriter.write("<p>"+"Collection : "+"false"+"</p>");
                            }
                            if(parameter.isNullable()){
                                bufferedWriter.write("<p>"+"Nullable : "+"true"+"</p>");
                            }
                            else {
                                bufferedWriter.write("<p>"+"Nullable : "+"false"+"</p>");
                            }
                            bufferedWriter.write("<p style=\"text-indent: 120px\">}</p>");
                        }
                    }
                    bufferedWriter.write("<p>"+"],</p>");
                    bufferedWriter.write("<p> Execute : {</p>");
                    if(action.getExecute().getSQL() != null) {
                        bufferedWriter.write("<p id=\"secForSQL\"> SQL : {</p>");
                        String where = action.getExecute().getSQL().getWhere();

                        if(where != null){
                            bufferedWriter.write("<p id=\"sec4\">"+"Where : "+where+"</p>");
                        }
                        else {
                            bufferedWriter.write("<p id=\"sec4\">"+"From : "+action.getExecute().getSQL().getFrom()+"</p>");
                        }
                        bufferedWriter.write("<p id=\"sec4\">"+"Bind : [</p>");
                        bufferedWriter.write("<div id=\"sec5\">");
                        for(Bind bind : action.getExecute().getSQL().getBind()){
                            bufferedWriter.write("<p style=\"text-indent: 190px\">}</p>");
                            bufferedWriter.write("<p>"+"Kind : "+bind.getKind()+"</p>");
                            bufferedWriter.write("<p>"+"Name : "+bind.getName()+"</p>");
                            bufferedWriter.write("<p>"+"ImplementationType : "+bind.getImplementationType()+"</p>");
                            bufferedWriter.write("<p style=\"text-indent: 190px\">}</p>");
                        }
                        bufferedWriter.write("</div>");
                        bufferedWriter.write("<p id=\"sec4\">]</p>");
                        bufferedWriter.write("<p id=\"secForSQL\">}</p>");
                    }
                    if(action.getExecute().getPLSQL() != null){
                        bufferedWriter.write("<p id=\"secForSQL\"> PL-SQL : {</p>");
                        bufferedWriter.write("<p id=\"sec4\">"+"Code : [</p>");
                        for (String code:action.getExecute().getPLSQL().getCode()){
                            bufferedWriter.write("<div id=\"sec5\">");
                            bufferedWriter.write("<p>"+code+"</p>");
                            bufferedWriter.write("</div>");
                        }
                        bufferedWriter.write("<p id=\"sec4\">],</p>");
                        bufferedWriter.write("<p id=\"sec4\">"+"Bind : [</p>");
                        bufferedWriter.write("<div id=\"sec5\">");
                        for(Bind bind : action.getExecute().getPLSQL().getBind()){
                            bufferedWriter.write("<p style=\"text-indent: 190px\">}</p>");
                            bufferedWriter.write("<p>"+"Kind : "+bind.getKind()+"</p>");
                            bufferedWriter.write("<p>"+"Name : "+bind.getName()+"</p>");
                            bufferedWriter.write("<p>"+"ImplementationType : "+bind.getImplementationType()+"</p>");
                            bufferedWriter.write("<p style=\"text-indent: 190px\">}</p>");
                        }
                        bufferedWriter.write("</div>");
                        bufferedWriter.write("<p id=\"sec4\">]</p>");
                        bufferedWriter.write("<p id=\"secForSQL\">}</p>");
                    }
                    bufferedWriter.write("<p>}</p>");
                    //end execute
                    bufferedWriter.write("</div>");
                    bufferedWriter.write("<p>},</p>");
                    bufferedWriter.write("</div>");
                }
                bufferedWriter.write("<p id=\"sec2\">],</p>");
                //end of action
                //start functions
                bufferedWriter.write("<p id=\"sec2\">Functions : [</p>");
                bufferedWriter.write("<div id=\"sec3\">");
                for (Function function : entityType.getFunctions()) {
                    int countForName = 0;
                    String functionName = function.getName();
                    for (String error : function.getErrorList()) {
                        StringTokenizer stringTokenizer = new StringTokenizer(error, "/");
                        String part = stringTokenizer.nextToken();
                        if (part.equals(functionName)) {
                            countForName++;
                        }
                    }
                    bufferedWriter.write("<p style=\"text-indent: 75px\">{</p>");
                    if (countForName > 0) {
                        bufferedWriter.write("<p>" + "Name : " + "<span style=\"color:red;\">" + functionName + "</span>" + "</p>");
                    } else {
                        bufferedWriter.write("<p>" + "Name : " + functionName + "</p>");
                    }
                    bufferedWriter.write("<p>" + "DataType : " + function.getDataType() + "</p>");
                    bufferedWriter.write("<p>" + "Implementation : " + function.getImplementation() + "</p>");
                    bufferedWriter.write("<p>" + "Bean : " + function.getBean() + "</p>");
                    bufferedWriter.write("<p>" + "SubType : " + function.getSubType() + "</p>");
                    if (function.isCollection()) {
                        bufferedWriter.write("<p>" + "Collection : " + "true" + "</p>");
                    } else {
                        bufferedWriter.write("<p>" + "Collection : " + "false" + "</p>");
                    }
                    bufferedWriter.write("<p> Parameters : [</p>");
                    bufferedWriter.write("<div id=\"sec4\">");
                    for (Parameter parameter : function.getParameters()) {
                        bufferedWriter.write("<p style=\"text-indent: 120px\">{</p>");
                        bufferedWriter.write("<p>" + "Name : " + parameter.getName() + "</p>");
                        bufferedWriter.write("<p>" + "DataType : " + parameter.getDataType() + "</p>");
                        if (parameter.isCollection()) {
                            bufferedWriter.write("<p>" + "Collection : " + "true" + "</p>");
                        } else {
                            bufferedWriter.write("<p>" + "Collection : " + "false" + "</p>");
                        }
                        if (parameter.isNullable()) {
                            bufferedWriter.write("<p>" + "Nullable : " + "true" + "</p>");
                        } else {
                            bufferedWriter.write("<p>" + "Nullable : " + "false" + "</p>");
                        }
                        bufferedWriter.write("<p style=\"text-indent: 120px\">}</p>");
                    }

                    bufferedWriter.write("</div>");
                    bufferedWriter.write("<p>]</p>");
                    bufferedWriter.write("<p> Execute : {</p>");
                    bufferedWriter.write("<p id=\"secForSQL\"> SQL : {</p>");
                    String where = function.getExecute().getSQL().getWhere();
                    if (where != null && countForName > 0) {
                        bufferedWriter.write("<p id=\"sec4\">" + "Where : " + "<span style=\"color:red;\">" + where + "</span></p>");
                    } else if (where != null) {
                        bufferedWriter.write("<p id=\"sec4\">" + "Where : " + where + "</p>");
                    } else {
                        bufferedWriter.write("<p id=\"sec4\">" + "From : " + function.getExecute().getSQL().getFrom() + "</p>");
                    }
                    bufferedWriter.write("<p id=\"sec4\">" + "Bind : [</p>");
                    bufferedWriter.write("<div id=\"sec5\">");
                    for (Bind bind : function.getExecute().getSQL().getBind()) {
                        bufferedWriter.write("<p style=\"text-indent: 190px\">}</p>");
                        bufferedWriter.write("<p>" + "Kind : " + bind.getKind() + "</p>");
                        bufferedWriter.write("<p>" + "Name : " + bind.getName() + "</p>");
                        bufferedWriter.write("<p>" + "ImplementationType : " + bind.getImplementationType() + "</p>");
                        bufferedWriter.write("<p style=\"text-indent: 190px\">}</p>");
                    }
                    bufferedWriter.write("</div>");
                    bufferedWriter.write("<p id=\"sec4\">]</p>");
                    bufferedWriter.write("<p id=\"secForSQL\">}</p>");
                    bufferedWriter.write("<p>}</p>");

                    bufferedWriter.write("<p style=\"text-indent: 75px\">},</p>");
                    bufferedWriter.write("<br>");
                }

                bufferedWriter.write("</div>");
                bufferedWriter.write("<br>");
                bufferedWriter.write("<p id=\"sec2\">]</p>");
                bufferedWriter.write("<p style=\"text-indent: 40px\">},</p>");
            }
            bufferedWriter.write("<br>");
            //end of Entities

            bufferedWriter.write("</div>");
            bufferedWriter.write("<p id=\"sec1\">]</p>");
            bufferedWriter.write("<br>");
            //end of projection
            bufferedWriter.write("}");
            bufferedWriter.write("<br>");
            bufferedWriter.write("</div>");
            bufferedWriter.write("<br>");
            bufferedWriter.write("</div>");
            bufferedWriter.write("<br>");
            //table report
            ProjectionRule rule ;
            ArrayList<Result> list;
            bufferedWriter.write("<div style=\"background-color:#FFFFFF;\">");

            String header="<div style=\"font-weight: bold\"><center><h2>PROJECTION VALIDATION REPORT</h2></center></div>";
            bufferedWriter.write(header);
            String image="<div><img src=\"src\\main\\java\\outputGenerator\\image.jpg\" alt=\"violations\" ></div>";
            bufferedWriter.write(image);
            bufferedWriter.write("<TABLE BORDER style=\"width:100%\"><TR style=\"background-color: black; color:white;\" ><TH>RuleName</TH><TH>Description</TH><TH>Error Message</TH></TR>");
            for (HashMap.Entry<ProjectionRule,ArrayList<Result>> entry : rulesAndResults.entrySet()) {

                rule = entry.getKey();
                String RuleName = rule.getName();
                String description = rule.getDescription();
                list = entry.getValue();
                if(!list.isEmpty()) {
                    bufferedWriter.write("<TR style=\"background-color:  #e6e6e6;\" ><TD align=\"center\">" + RuleName + "<TD>"
                            + description);
                    bufferedWriter.write("<TD  style=\" color:red;\" rowspan='"+list.size()+"' >");


                    for (Result r : list) {

                        bufferedWriter.write("code :  "+r.getCode()+",   "+r.getMessage());
                        bufferedWriter.write("</BR>");

                    }

                    bufferedWriter.write("<TR>");


                }

            }
            bufferedWriter.write("</TABLE>");
            bufferedWriter.write("<br>");

            //end of report
            bufferedWriter.write("</div>");
            bufferedWriter.write("<br>");


            bufferedWriter.write("</div>");

            bufferedWriter.write("</body>");
            bufferedWriter.write("</html>");
            bufferedWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
