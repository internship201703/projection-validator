package rules;


import com.ifsworld.fnd.metadata.cache.model.dto.EntityType;
import com.ifsworld.fnd.metadata.cache.model.dto.Function;
import com.ifsworld.fnd.metadata.cache.model.dto.Projection;
import result.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
/**
 * Created by wasana lakmali on 15/12/2017.
 */

public class Rule04 implements ProjectionRule {


    private int code = 1;
    private String message;
    private String name = "Rule04";
    private String description ="Function is allow to have where clause or not ";
    ArrayList<Result> list = new ArrayList<Result>();

    @Override
    public ArrayList<Result> run(Projection projection) {

        ArrayList<Function> functions = (ArrayList<Function>) projection.getContainer().getFunctions();
        ArrayList<EntityType> entityTypes = (ArrayList<EntityType>) projection.getEntities();
        String functionName;
        String where;
        for(Function function:functions){

            where =function.getExecute().getSQL().getWhere();

            if(where !=null){

                if(function.isCollection()!=true &&  !function.getDataType().equals("Entity")){

                    functionName=function.getName();

                    message = "Function : "+functionName +" ,"+"  should not have where condition  ";
                    Result result = new Result(code,message);
                    result.setErrorMessage(functionName+"/"+message);
                    result.setObject(function);
                    list.add(result);




                }
            }



        }
        for(EntityType entityType:entityTypes){

            List<Function> entityFunctions = entityType.getFunctions();

            for(Function function:entityFunctions) {

                where =function.getExecute().getSQL().getWhere();
                if(where !=null){

                    if(function.isCollection()!=true &&  !function.getDataType().equals("Entity")){

                        functionName=function.getName();

                        message = "Function : "+functionName +" ,"+"  should not have where condition  ";
                        Result result = new Result(code,message);
                        result.setErrorMessage(functionName+"/"+message);
                        result.setObject(function);
                        list.add(result);



                    }
                }

            }

        }
        return list;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }


}
