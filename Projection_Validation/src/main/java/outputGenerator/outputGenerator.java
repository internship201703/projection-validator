package outputGenerator;

import com.ifsworld.fnd.metadata.cache.model.dto.Projection;
import result.Result;
import rules.ProjectionRule;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Sajith Jayasekara on 7/15/2017.
 */
public interface outputGenerator {

    public void printOutput(Projection projection,HashMap<ProjectionRule, ArrayList<Result>> rulesAndResults);
}
