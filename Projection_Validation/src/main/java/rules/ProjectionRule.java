package rules;


import com.ifsworld.fnd.metadata.cache.model.dto.Projection;
import result.Result;

import java.util.ArrayList;

/**
 * Created by Sajith Jayasekara on 7/11/2017.
 */
public interface ProjectionRule {

    public ArrayList<Result> run(Projection projection);
    public String getName();
    public String getDescription();
}
