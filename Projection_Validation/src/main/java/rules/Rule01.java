package rules;


import com.ifsworld.fnd.metadata.cache.model.dto.Projection;
import result.Result;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Sajith Jayasekara on 7/11/2017.
 */
public class Rule01 implements ProjectionRule {

    private int code = 1;
    private String message;
    private String name = "Rule01";
    private String description = "Projection name's first letter should be Uppercase and length must be less than 30";
    private ArrayList<Result> list = new ArrayList<Result>();



    @Override
    public ArrayList<Result> run(Projection projection) {

        String name = projection.getName();
        char array[] = name.toCharArray();
        int length = name.length();
        int count = 0;
        int underScore=1;
        int SVC=3;


        if(!Character.isUpperCase(name.charAt(0))){
            message = "Projection name : "+name+" ,"+ " first letter should be Uppercase  ";
            Result result = new Result(code,message);
            result.setErrorMessage(name+"/"+message);
            result.setObject(projection);
            list.add(result);


        }

        for (char ch:array){
            if(Character.isUpperCase(ch)){
                count++;

            }
        }
        underScore=underScore*count;
        length=length+underScore+SVC;

        if(length>30){
            message = " Projection name : "+name+" ,"+ " identifier length should less than 30 ";
            Result result = new Result(code,message);
            result.setErrorMessage(name+"/"+message);
            result.setObject(projection);
            list.add(result);
        }


        return list;

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }


}
