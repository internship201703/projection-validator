package result;

import java.io.*;

/**
 * Created by Sajith Jayasekara on 7/11/2017.
 */
public class Result {

    private int code;
    private String Message;
    private String errorMessage;
    private Object object;

    public Result(int code, String message) {
        this.code = code;
        Message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return Message;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
