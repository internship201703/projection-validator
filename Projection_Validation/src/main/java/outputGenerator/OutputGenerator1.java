package outputGenerator;

import com.ifsworld.fnd.metadata.cache.model.dto.Projection;
import result.Result;
import rules.ProjectionRule;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Sajith Jayasekara on 9/12/2017.
 */
public class OutputGenerator1 implements outputGenerator {
    private ProjectionRule rule;
    private ArrayList<Result> list;
    @Override
    public void printOutput(Projection projection,HashMap<ProjectionRule, ArrayList<Result>> rulesAndResults) {

        File file=new File("NewOutput1.html");

        if(file.exists()){
            file.delete();
        }
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            bufferedWriter.write("<html>");
            bufferedWriter.write("<head>");
            bufferedWriter.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            String header="<div><center><h1 style=\"color:#00b33c;\">Projection Validation Report!</h1></center></div>";
            bufferedWriter.write(header);
            bufferedWriter.newLine();
            bufferedWriter.newLine();
            String image="<div><img src=\"src\\main\\java\\outputGenerator\\image.jpg\" alt=\"violations\" ></div>";
            bufferedWriter.write(image);
            bufferedWriter.newLine();
            bufferedWriter.newLine();
            bufferedWriter.write("<TABLE BORDER style=\"width:100%\"><TR style=\"background-color: black; color:white;\" ><TH>RuleName</TH><TH>Description</TH><TH>Error Message</TH></TR>");
            for (HashMap.Entry<ProjectionRule,ArrayList<Result>> entry : rulesAndResults.entrySet()) {

                rule = entry.getKey();
                String RuleName = rule.getName();
                String description = rule.getDescription();
                list = entry.getValue();
                if(!list.isEmpty()) {
                    bufferedWriter.write("<TR style=\"background-color:  #e6e6e6;\" ><TD align=\"center\">" + RuleName + "<TD>"
                            + description);
                    bufferedWriter.write("<TD  style=\" color:red;\" rowspan='"+list.size()+"' >");


                    for (Result r : list) {

                        bufferedWriter.write("code :  "+r.getCode()+",   "+r.getMessage());
                        bufferedWriter.write("</BR>");

                    }

                    bufferedWriter.write("<TR>");


                }
            }
            bufferedWriter.write("</body>");
            bufferedWriter.write("</html>");
            bufferedWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
