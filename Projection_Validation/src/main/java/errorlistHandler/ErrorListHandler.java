package errorlistHandler;

import result.Result;
import rules.ProjectionRule;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Sajith Jayasekara on 9/12/2017.
 */
public class ErrorListHandler {


    public void handler(HashMap<ProjectionRule,ArrayList<Result>> rulesAndResults){

        for(HashMap.Entry<ProjectionRule,ArrayList<Result>> ruleArrayListEntry:rulesAndResults.entrySet()){

            for(Result result:ruleArrayListEntry.getValue()){

                Class anonymousClass = result.getObject().getClass();
                try {
                    Method method = anonymousClass.getMethod("getErrorList");
                    ArrayList<String> list = (ArrayList<String>) method.invoke(result.getObject());
                    list.add(result.getErrorMessage());

                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
